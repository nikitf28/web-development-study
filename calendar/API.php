<?php

namespace app;

use mysql_xdevapi\Warning;

include('DbConnector.php');

session_start();
if (!isset($_SESSION['user_id']) or $_SESSION['user_id'] == -1){
    http_response_code(401);
    echo json_encode(array("message" => "401 - Need authorisation!"), JSON_UNESCAPED_UNICODE);
}

function get_tasks_to_do($db, $user_id){
    $result = prepare_tasks($db->get_tasks_to_do($user_id));
    http_response_code(200);
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
}

function get_tasks_succeed($db, $user_id){
    $result = prepare_tasks($db->get_tasks_succeed($user_id));
    http_response_code(200);
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
}

function get_tasks_deadlined($db, $user_id){
    $result = prepare_tasks($db->get_tasks_deadlined($user_id));
    http_response_code(200);
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
}

function get_tasks_for_specific_date($db, $user_id, $datetime){
    $result = prepare_tasks($db->get_tasks_for_specific_date($user_id, $datetime));
    http_response_code(200);
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
}
function get_tasks_for_id($db, $user_id, $task_id){
    $result = prepare_tasks($db->get_tasks_for_id($user_id, $task_id));
    http_response_code(200);
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
}

function init_db($db){
    $db->init_tables();
    http_response_code(200);
    echo json_encode(array("message" => "Ok!"), JSON_UNESCAPED_UNICODE);
}

function prepare_tasks($tasks): array
{
    $tasks_ans = [];
    foreach ($tasks as $key => $task) {
        $tasks_ans[] = $task->get_dict();
    }
    return $tasks_ans;
}

header('Content-type: application/json');

if (! $_POST){
    http_response_code(400);
    echo json_encode(array("message" => "400 - Bad request, POST need!"), JSON_UNESCAPED_UNICODE);
}

if (! key_exists('method', $_POST)){
    http_response_code(400);
    echo json_encode(array("message" => "400 - Bad request, need method!"), JSON_UNESCAPED_UNICODE);
    return;
}

$method = $_POST['method'];

$ini = parse_ini_file('config.ini');
$db_host = $ini['db_host'];
$db_name = $ini['db_name'];
$db_login = $ini['db_login'];
$db_password = $ini['db_password'];

$db_connector = new DbConnector($db_host, $db_name, $db_login, $db_password);

if ($method == 'init_db'){
    init_db($db_connector);
    return;
}
$global_user_id = $_SESSION['user_id'];
if ($method == 'get_tasks_to_do'){
    get_tasks_to_do($db_connector, $global_user_id);
    return;
}
if ($method == 'get_tasks_succeed'){
    get_tasks_succeed($db_connector, $global_user_id);
    return;
}
if ($method == 'get_tasks_deadlined'){
    get_tasks_deadlined($db_connector, $global_user_id);
    return;
}
if ($method == 'get_tasks_for_specific_date'){
    if (! key_exists('datetime', $_POST)){
        http_response_code(400);
        echo json_encode(array("message" => "400 - Bad request, need datetime!"), JSON_UNESCAPED_UNICODE);
        return;
    }
    get_tasks_for_specific_date($db_connector, $global_user_id, $_POST['datetime']);
    return;
}
if ($method == 'get_tasks_for_id'){
    if (! key_exists('task_id', $_POST)){
        http_response_code(400);
        echo json_encode(array("message" => "400 - Bad request, need task_id!"), JSON_UNESCAPED_UNICODE);
        return;
    }
    get_tasks_for_id($db_connector, $global_user_id, $_POST['task_id']);
    return;
}
if ($method == 'save_task'){
    $fields = ['id', 'topic', 'type', 'location', 'datetime', 'duration', 'comment', 'status'];
    $numeric_fileds = ['id', 'type', 'datetime', 'duration', 'status'];
    foreach ($fields as $field) {
        if (! key_exists($field, $_POST)){
            http_response_code(400);
            echo json_encode(array("message" => "400 - Bad request, need $field!"), JSON_UNESCAPED_UNICODE);
            return;
        }
    }
    foreach ($numeric_fileds as $numeric_filed){
        if (! is_numeric($_POST[$numeric_filed])){
            http_response_code(400);
            echo json_encode(array("message" => "400 - Bad request, $numeric_filed is not numeric!"), JSON_UNESCAPED_UNICODE);
            return;
        }
    }
    if (strlen($_POST['location']) > 100){
        http_response_code(400);
        echo json_encode(array("message" => "400 - Bad request, too long location!"), JSON_UNESCAPED_UNICODE);
        return;
    }
    if (strlen($_POST['topic']) > 100){
        http_response_code(400);
        echo json_encode(array("message" => "400 - Bad request, too long topic!"), JSON_UNESCAPED_UNICODE);
        return;
    }
    if (strlen($_POST['comment']) > 5000){
        http_response_code(400);
        echo json_encode(array("message" => "400 - Bad request, too long comment!"), JSON_UNESCAPED_UNICODE);
        return;
    }
    $task = new Task(
        htmlspecialchars($_POST['id']),
        htmlspecialchars($_POST['topic']),
        htmlspecialchars($_POST['type']),
        htmlspecialchars($_POST['location']),
        htmlspecialchars(date('Y-m-d H:i:s', intval($_POST['datetime']))),
        htmlspecialchars($_POST['duration']),
        htmlspecialchars($_POST['comment']),
        htmlspecialchars($_POST['status']),
        $global_user_id,
    );
    if ($_POST['id'] == '0'){
        $db_connector->add_task($task);
        //error_log(print_r($task->get_dict(), TRUE));
    }
    else{
        $db_connector->edit_task($task);
    }
    http_response_code(200);
    echo json_encode(array("message" => "Ok!"), JSON_UNESCAPED_UNICODE);
}