<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Регулярные выражения</title>
    </head>
    <body>
        <?php
            function test_reg($name, $strs, $reg){
                $answers = [0 => '-', 1 => '+'];
                echo  '<b>' . $name . ':</b>'. '<br>';
                echo $reg . '<br>';
                foreach ($strs as $key => $value){
                    echo $value . ': ' . $answers[preg_match($reg, $value)] . "<br>";
                }
                echo "===========<br>";
            }
            $reg = '/^[0-9]+$/';
            test_reg('Целое число', ['1', '1.5', 'asdasdasd', '324552345', '232,623'], $reg);
            $reg = '/^[A-z0-9]+$/';
            test_reg('Буквы (латиница) и цифры', ['1', '1.5', 'asdaSDDDWsdasd', '324552345', '232,623', 'asdasd', '32r22e3', 'dasd@ya.ru', 'тест'], $reg);
            $reg = '/^[\x{0410}-\x{044F}A-z0-9]+$/u';
            test_reg('Буквы (латиница и кириллица) и цифры',    ['1', '1.5', 'asdasdasd', '324552345', '232,623', 'asdasd', '32r22e3', 'dasd@ya.ru', 'выфвфвф'], $reg);
            $reg = '/^[\x{0410}-\x{044F}A-z0-9]+$/u';
            test_reg('Домен',    ['1', '1.5', 'asdasdasd', '324552345', '232,623', 'yandex.ru', '32r22e3', 'dasd@ya.ru', 'bki.forlabs.com'], $reg);

        ?>
    </body>
</html>