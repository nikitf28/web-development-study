<!DOCTYPE html>

<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Админка</title>
        <link rel="stylesheet" type="text/css" href="css/registration.css">
        <link rel="stylesheet" type="text/css" href="./css/checkbox2.css">
        <link rel="stylesheet" type="text/css" href="./css/admin.css">
    </head>
    <body>
        <div class="form_box">
            <h1>Управление записями</h1>
            <form method="post" action="admin.php">
                <div class="scroll_box">
                    <?php
                    $files = scandir('answers');
                    if ($_POST){
                        foreach ($files as $key => $value){
                            if (!in_array($value,array(".",".."))){
                                if (array_key_exists(substr($value, 0, -4).'_txt', $_POST)){
                                    if ($_POST[substr($value, 0, -4).'_txt'] == 'on'){
                                        unlink('answers/'.substr($value, 0, -4).'.txt');
                                    }
                                }
                            }
                        }

                        header('Location: admin.php', true, 303);
                    }
                    $files = scandir('answers');
                    foreach ($files as $key => $value){
                        if (!in_array($value,array(".",".."))){
                            $file_data = file_get_contents('answers/'.$value);
                            $data = json_decode($file_data);
                            $first_name = $data->first_name;
                            $second_name = $data->second_name;
                            $email = $data->email;
                            $phone = $data->phone;
                            $topic = $data->topic;
                            $payment_method = $data->payment_method;
                            $subscription = $data->subscription;
                            if ($subscription == 'on'){
                                $subscription = 'Да';
                            }
                            else{
                                $subscription = 'Нет';
                            }
                            $registration_time = date_create_from_format('Y-m-d_H-i-s', $data->date_time)->format('d.m.Y H:i');
                            echo sprintf(
                                '<div class="record_box">
                                        <div class="left_colum">
                                            <input name="%s" type="checkbox" id="%s" style="display:none"/>
                                            <label for="%s" class="toggle"><div class="tick_mark"></div></label>
                                        </div>
                                        <div class="right_column">
                                            <div class="name line">
                                                %s <span class="second_name">%s</span>
                                            </div>
                                            <div class="contact line">
                                                %s / %s
                                            </div>
                                            <div class="topic line">
                                                Тема: %s
                                            </div>
                                            <div class="payment line">
                                                Способ оплаты: %s
                                            </div>
                                            <div class="subscription line">
                                                Рассылка: %s
                                            </div>
                                            <div class="registration_date line">
                                                Дата регистрации: %s
                                            </div>
                                        </div>
                                    </div>',
                                $value, $value, $value, $first_name, $second_name, $phone, $email, $topic, $payment_method,
                                $subscription, $registration_time);
                        }
                    }
                    ?>
                </div>
                <input id="submit" type="submit" value="Удалить выбранные" name="submit">
            </form>

        </div>
    </body>
</html>
