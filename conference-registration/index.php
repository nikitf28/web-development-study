<!DOCTYPE html>

<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Регистрация на конференцию</title>
        <link rel="stylesheet" type="text/css" href="css/registration.css">
        <link rel="stylesheet" type="text/css" href="./css/radio.css">
        <link rel="stylesheet" type="text/css" href="./css/checkbox.css">
    </head>
    <body>
    <?php
        $information = array();
        $fields = array('first_name' => "", 'second_name' => "", 'email' => "", 'phone' => "", 'topic' => "",
            'payment_method' => "", 'subscription' => "");
        $topic = array("Бизнес" => "", "Технологии" => "", "Реклама и Маркетинг" => "");
        $payment_method = array("WebMoney" => "", "Яндекс.Деньги" => "", "PayPal" => "", "Банковская карта" => "");
        $error = false;
        $subscription = '';
        if ($_POST){
            foreach ($fields as $key => $value){
                if (array_key_exists($key, $_POST)){
                    $fields[$key] = htmlspecialchars($_POST[$key]);
                }
                if ($key != 'subscription' and $key == ''){
                    $error = true;
                }
                if (strlen($_POST[$key]) > 50){
                    $error= true;
                }
            }
            if (array_key_exists($fields['payment_method'], $payment_method)){
                $payment_method[$fields['payment_method']] = "checked";
            }
            if (array_key_exists($fields['topic'], $topic)){
                $topic[$fields['topic']] = "checked";
            }
            if ($fields['subscription'] == "on"){
                $subscription = "checked";
            }
            $fields['date_time'] = (new DateTime())->format('Y-m-d_H-i-s');
            if (!$error){
                $file_name = 'answers/'.$fields['date_time'].'_'.bin2hex(random_bytes(5)).'.txt';
                file_put_contents($file_name, json_encode($fields));
                header('Location: success.html', true, 303);
            }

        ?>
        <?php
            if ($error){
                echo "<style>.form_box .input_line:last-child{
                                    display: block;
                              }</style>";
                }
            }
        ?>
        <div class="form_box">
            <h1>Регистрация на конференцию</h1>
                <form action="index.php" method="post">
                    <div class="input_line">
                        <img alt="user_icon" class="form_icon" src="icons/user.svg">
                        <input name="first_name" placeholder="Имя" type="text" maxlength="50" value="<?php echo $fields['first_name'];?>" required>
                    </div>
                    <div class="input_line">
                        <img alt="user_icon" class="form_icon" src="icons/user.svg">
                        <input name="second_name" placeholder="Фамилия" type="text" maxlength="50" value="<?php echo $fields['second_name'];?>" required>
                    </div>
                    <div class="input_line">
                        <img alt="mail_icon" class="form_icon" src="icons/mail.svg">
                        <input name="email" placeholder="Email" type="email" maxlength="50" value="<?php echo $fields['email'];?>" required>
                    </div>
                    <div class="input_line">
                        <img alt="phone_icon" class="form_icon" src="icons/phone.svg">
                        <input name="phone" placeholder="Номер телефона" type="tel" maxlength="50" value="<?php echo $fields['phone'];?>" required>
                    </div>
                    <div class="selection_wrapper input_line">
                        <img alt="topic_icon" class="form_icon" src="icons/topic.svg">
                        <div class="selection">
                            <div class="header">Тема конференции:</div>
                            <div class="radio">
                                <input id="c1" type="radio" value="Бизнес" name="topic" required <?php echo $topic["Бизнес"];?>>
                                <label class="radio-label" for="c1">Бизнес</label>
                            </div>
                            <div class="radio">
                                <input id="c2" type="radio" value="Технологии" name="topic" required <?php echo $topic["Технологии"];?>>
                                <label class="radio-label" for="c2">Технологии</label>
                            </div>
                            <div class="radio">
                                <input id="c3" type="radio" value="Реклама и Маркетинг" name="topic" required <?php echo $topic["Реклама и Маркетинг"];?>>
                                <label class="radio-label" for="c3">Реклама и Маркетинг</label>
                            </div>

                        </div>
                    </div>
                    <div class="selection_wrapper input_line">
                        <img alt="money_icon" class="form_icon" src="icons/dollar.svg">
                        <div class="selection">
                            <div class="header">Метод оплаты участия:</div>
                            <div class="radio">
                                <input id="p1" type="radio" value="WebMoney" name="payment_method" required <?php echo $payment_method["WebMoney"];?>>
                                <label class="radio-label" for="p1">WebMoney</label>
                            </div>
                            <div class="radio">
                                <input id="p2" type="radio" value="Яндекс.Деньги" name="payment_method" required <?php echo $payment_method["Яндекс.Деньги"];?>>
                                <label class="radio-label" for="p2">ЮMoney (бывш. Яндекс.Деньги)</label>
                            </div>
                            <div class="radio">
                                <input id="p3" type="radio" value="PayPal" name="payment_method" required <?php echo $payment_method["PayPal"];?>>
                                <label class="radio-label" for="p3">PayPal</label>
                            </div>
                            <div class="radio">
                                <input id="p4" type="radio" value="Банковская карта" name="payment_method" required <?php echo $payment_method["Банковская карта"];?>>
                                <label class="radio-label" for="p4">Банковская карта (VISA, MasterCard, МИР)</label>
                            </div>
                        </div>
                    </div>
                <div class="input_line cb_line">
                    <img alt="notification_icon" class="form_icon" src="icons/bell.svg">
                    <input name="subscription" type="checkbox" id="cb1" style="display:none" <?php echo $subscription;?>/>
                    <label for="cb1" class="toggle"><div class="tick_mark"></div></label>
                    <div class="cb_label">Хочу получать рассылку о конференции</div>
                </div>
                <div class="input_line">
                    <input id="submit" type="submit" value="Зарегистрироваться" name="submit">
                </div>
                <div id="error" class="input_line">
                   Не все поля заполнены, попробуйте ещё раз
                </div>

            </form>
        </div>

    </body>
</html>

