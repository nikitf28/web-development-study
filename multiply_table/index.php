<html>
    <head>
        <title>Таблица умножения</title>
        <link rel="stylesheet" href="./css/styles.css">
    </head>
    <body>
        <table class='multiply_table'>
        <?php
            for ($i = 0; $i <= 10; $i++){
                echo "<tr>";
                for ($j = 0; $j <= 10; $j++){
                    if ($i == 0){
                        if ($j == 0){
                            echo "<td class='cell top left'>".$j."</td>";
                            continue;
                        }
                        echo "<td class='cell top'>".$j."</td>";
                        continue;
                    }
                    if ($j == 0){
                        echo "<td class='cell left'>".$i."</td>";
                        continue;
                    }
                    if ($i == $j){
                        echo "<td class='cell diagonal'>".$i*$j."</td>";
                        continue;
                    }
                    echo "<td class='cell'>".$i*$j."</td>";
                }
                echo "</tr>";
            }
        ?>
        </table>
    </body>
</html>
